from setuptools import setup

setup(
    name='ThreeSidedCoin',
    version='0.2',
    description='A pybullet simulation to find optimal 3-sided coin geometry.',
    author='timjolson',
    packages=[],
    install_requires=['numpy','pybullet','tqdm','matplotlib', 'scipy'],
)
